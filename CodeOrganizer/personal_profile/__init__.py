import flask_login
from flask import Blueprint, request
from wtforms import Form, FormField, URLField, BooleanField, StringField, FieldList, TextAreaField PasswordField, validators, widgets

personal_profile = Blueprint(
    "personal_profile", __name__, template_folder="../../templates")


class SocialForm(Form):
    name = StringField("Name", [validators.DataRequired()])
    url = URLField("URL", [validators.DataRequired()])


class EditAccount(Form):
    bio = TextAreaField("Bio", [validators.Length(min=1, max=500)])
    socials = FieldList(FormField(SocialForm))


@personal_profile.route("/edit", methods=["GET", "POST"])
@flask_login.login_required
def edit():
    profile = flask_login.current_account

    formdate = {
        "bio": profile.bio,
        "socials": [SocialForm({"name": n, "url": profile.socials[n]}) for n in profile.socials]
    }

    form = EditAccount(request.form, **formdata)

    if request.method == "POST" and form.validate():
        if form.bio.data:
            profile.bio = form.bio.data
        if form.socials.data:
            for social in form.socials.data:
                profile.socials[social.name] = social.url

        profile.save()
        return redirect(url_for("edit"))

    return render_template("edit_personal_profile.html", form=form)
