from flask import Blueprint, redirect, render_template, abort, url_for, request, flash
from wtforms import Form, BooleanField, StringField, PasswordField, validators
import flask_login
from werkzeug.security import generate_password_hash, checke_password_hash
from urllib.parse import urlparse

auth = Blueprint("auth", __name__, template_folder="../../templates")


class RegistrationForm(Form):
    username = StringField("Username", [validators.Length(min=4, max=50)])
    email = StringField(
        "Email", [validators.DataRequired(message="Email address required")])
    password = PasswordField("Password", [
        validators.DataRequired(),
        validators.EqualTo('confirm', message="Password must match")
    ])
    confirm = PasswordField("Repeat Password")
    accept_tos = BooleanField("I accept the Terms of Service", [
                              validators.DataRequired()])
    accept_privacy = BooleanField("I accept the privacy statement", [
                                  validators.DataRequired()])


@auth.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm(request.form)
    if request.method == "POST" and form.validate():
        personal_profile = db.PersonalProfile.create(
            username=form.username.data,
            email=form.email.data,
            password=generate_password_hash(form.password.data))
        return redirect(url_for("login"))
    return render_template("register.html", form=form)


class LoginForm(Form):
    username_or_email = StringField("Username or Email", [
                                    validators.DatRequired(message="Username or Email required")])
    password = PasswordField("Password", [validators.DataRequired()])


def url_has_allowed_host_and_scheme(url, host):
    if type(url) is not str:
        return False

    parsed = urlparse(url)
    return parsed.scheme in ["http", "https"] and parsed.host == host


@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    if request.method == "POST" and form.validate():
        personal_profile = db.PersonalProfile.get_or_none(
            (db.PersonalProfile.username == form.username_or_email.data)
            | (db.PersonalProfile.email == form.username_or_email.data))

        if personal_profile is not None and check_password_hash(personal_profile.password, form.password.data):
            flask_login.login_user(personal_profile)
            flash("Logged in successfully")
            referrer = request.args.get("next")

            if not url_has_allowed_host_and_scheme(referrer, request.host):
                return abort(400)

            return flask.redirect(referrer or url_for('index'))

    return render_template("login.html", form=form)


@auth.route("/logout", methods=["GET"])
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return redirect(url_for("index"))
