from datetime import date, datetime
import peewee as pw
from playhouse.postgres_ext import JSONField, ArrayField, PostgresqlExtDatabase
import flask_login
from config import POSTGRES_USER, POSTGRES_HOST, POSTGRES_PASSWORD

db = PostgresqlExtDatabase(
    "app.db", user=POSTGRES_USER, host=POSTGRES_HOST, password=POSTGRES_PASSWORD)


class BaseModel(pw.Model):
    class Meta:
        database = db


class PersonalProfile(BaseModel, flask_login.UserMixin):
    id = pw.UUIDField(primary_key=True)
    username = pw.CharField(unique=True)
    email = pw.CharField(unique=True)

    password = pw.TextField()

    socials = JSONField(default=dict())
    bio = pw.CharField(max_length=500)


class ProjectProfile(BaseModel):
    id = pw.UUIDField(primary_key=True)
    name = pw.CharField()
    external_links = pw.CharField()
    description = pw.CharField(max_length=1000)


class Membership(BaseModel):
    """The relationship between a person and a project.
    A membership can either be active or inactive
    """
    project = pw.ForeignKeyField(ProjectProfile, backref="members")
    person = pw.ForeignKeyField(PersonalProfile, backref="projects")
    active = pw.BooleanField(default=True)
    joined_at = pw.DateField(default=date.today)
    last_active_at = pw.DateField(default=date.today)


class HistoryLogEntry(BaseModel):
    """People should easily read up about the progress and state of a project.

    It is thus paramount that projects keep a chronological history log.

    When editing a log entry, a new entry is created which supersedes the old.
    """
    entry = pw.CharField(max_length=1000)
    created_at = pw.DateTimeField(default=datetime.now)
    author = pw.ForeignKeyField(PersonalProfile, backref="history_log_entries")
    project = pw.ForeignKeyField(ProjectProfile, backref="history_log")
    replaced_by = pw.ForeignKeyField(
        "HistoryLogEntry", backref="previous_version", null=True)


class TopicOfInterest(BaseModel):
    """Any general or specific topic or area that a person might be interested in and that
    a project might generally be in.

    Multiple names for the same topic can exist, so all of these are here aliased.
    """
    id = pw.UUIDField(primary_key=True)
    names = ArrayField()

    personal_profiles = pw.ManyToManyField(
        PersonalProfile, backref="topics_of_interest")
    project_profiles = pw.ManyToManyField(
        ProjectProfile, backref="topics_of_interest")


class Skill(BaseModel):
    """Any skill/ability/technology/etc. a person might have or a project might require.

    Multiple names for the same skill can exist, so all of these are here aliased.
    """
    id = pw.UUIDField(primary_key=True)
    names = ArrayField()

    personal_profiles = pw.ManyToManyField(PersonalProfile, backref="skills")
    project_profiles = pw.ManyToManyField(
        ProjectProfile, backref="required_skills")


class Follow(BaseModel):
    """A basic relationship between to personal profiles.

    Some personal profiles may want to require approved follows only.
    """
    follower = pw.ForeignKeyField(PersonalProfile, backref="followings")
    followee = pw.ForeignKeyField(PersonalProfile, backref="followers")
    approved = pw.BooleanField(default=True)


def init_db():
    db.create_tables([
        PersonalProfile,
        ProjectProfile,
        Membership,
        HistoryLogEntry,
        TopicOfInterest,
        TopicOfInterest.personal_profiles.get_through_model(),
        TopicOfInterest.project_profiles.get_through_model(),
        Skill,
        Skill.personal_profiles.get_through_model(),
        Skill.project_profiles.get_through_model(),
        Follow
    ])
