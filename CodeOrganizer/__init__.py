from .db import db
from flask import Flask, render_template
from config import SECRET_KEY
from .profiles import profiles
import flask_login

app = Flask(__name__,
            static_folder="../static")
app.secret_key = SECRET_KEY
login_manager = flask_login.LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def user_loader(profile_id):
    return db.PersonalProfile.get_or_none(
        db.PersonalProfile.i == profile_id)


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")


app.register_blueprint(profiles, url_prefix="/profile")
