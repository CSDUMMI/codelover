# CodeLover

A project associated with [Guild Alpha](https://alpha.coding.social).

Tool to organize the FOSS Community by providing profiles, search functions and the ability to
gain new members and find new projects to work on *collectively*.

This project is written in Python with Flask, Jinja2 Templates and a Postgres Database.

## Profiles
There are two types of profiles on the platform:
1. Personal for people involved in FOSS development (incl. developers, designer, artists, translators,  etc.)
2. Project for both entire software projects (e.g. Mastodon, Bookwyrm, Forgejo), but also smaller features on these software projects.

### Personal Profiles
These can contain any amount of information that a person wants to provide about themselves
and that may help others find them for interesting projects and may help them find projects interesting for them.

Each personal profile contains:
1. A name (can be pseudonymous)
2. A variety of social links
3. A bio (max. 500 chars)
4. Topics of Interests: any project, technology, area, skill one might be interested in.
5. Projects: links to project profiles one has participated in.
6. Skills: any technology, skill, tool, etc. one is proficient/experienced/etc. in.
7. Follows: links to other personal profiles that one wants to work with.

### Project Profiles

A project profile is not owned by the creator of the project but instead organized by the Guild.

As such, any member on the platform can at any point *create* a project, but thereafter anyone can join the project and they remain in the project by their activity on it.

Most actions on a project, such as editing it's profile, can be performed by any member.
Only the action to delete or suspend a project requires a vote of active members.

A project profile consists of:
1. A name
2. A variety of forge links (to repositories, issues, PRs, etc.)
3. A description (max. 1000 chars)
4. Topics of Interest: any project, technology, area, skill *relating* to a project.
5. Members: Links to the personal profiles of past and present members
6. A history log: A log book of chronological entries about the project's progress
7. Skills: any technology, skill, tool, etc. *needed* on the project.

### Search Function
A search function should be created where personal and project profiles can be searched based on the fields describeb above.

### Suggestions
Both for members personally and for members on behalf of a project, there should be a "Suggestion" function allowing them to search for profiles (personal or project) that best fit their own profile.

# Examples
All abstract description aside, I'd like to play through a little bit of an example. Do the concept by hand, before coding it.
For this purpose I'll describe two profiles, a personal one for myself and a project one for this very project and then discuss how suggestions and searches would work.

## Personal Profile of CSDUMMI
Name: CSDUMMI
Social Links:
- Codeberg: codeberg.org/CSDUMMI
- GitLab: gitlab.com/CSDUMMI
- GitHub: github.com/CSDUMMI
- Fediverse: babka.social/@csdummi
Bio: A FOSS developer, active in Guild Alpha from the start. Developed a democratic social network, worked on Mastodon, Bookwyrm and other FOSS projects. Passionate abouth organized, democratic online communities.
Topics of Interest:
- NixOS
- Mastodon
- Federation
- Python
- Flask
- Haskell
- Elm
Projects: ->CodeOrganizer
Skills: Mastodon, Ruby on Rails, Flask, Python, JavaScript, Elm
Follows: []

## Project Profile of CodeOrganizer
Name: CodeOrganizer (Pls. help with the name)
Forge Links:
- Codeberg: codeberg.org/GuildAlpha/CodeOrganizer
Description: A tool for organizing the free software community collectively.
Topics of Interest:
- Backend Development
- Python
- MVC
- Responsive Design
- Elastic Search
- ML
- Frontend Development
Members: [->CSDUMMI]
History Log:
1. 2024-01-04: Created by CSDUMMI
2. 2024-01-04: Written extensive README as specification and proposal to the Guild.
Skills:
- Flask
- Python
- Postgresql
- Peewee
- Jinja2
- Codeberg API
- GitLab API
- GitHub API

### Suggestions
The suggestion algorithm has two modes:
1. Suggesting projects to people
2. Suggesting people for projects

But this is essentially the same thing, in the other direction.

So, let's start with the first mode:

When a person wants to be suggested a project, they press a button and:
1. All projects with at least one of their skills or points of interest are selected
2. All projects are removed from the list based on blocks
3. The dot product of a vector of optimization parameters and a vector describing the project profile is calculated and used to rank the projects. The project vector might contain:
    (1 for active project, -1 for suspended project)
    (number of active members)
    (size of the history log)
    (number of skills required by the project, present in the personal profile)
    (number of ToIs of the project, present in the personal profile)
    (number of active members in the project, who have cooperated on a project in the personal profile)
    (number of previous members in the project, who have cooperated on a project in the personal profile)
    (number of active members in the project, who the person follows)
    (number of previous members in the project, who the person follows)
4. Projects are ranked by their score

The optimization vector should be tuned to provide the best suggestions.

When a person wants to look for new members on a project, similar steps are performed for personal profiles:
1. All personal profiles with at least one of the required skills for the project selected
2. All personal profiles are removed based on blocks
3. The dot product of the profile vector and an optimization vector is calculated and used to sort personal profiles. The profile vector may contain:

    a tuned [logistic function](https://en.wikipedia.org/wiki/Logistic_function) of the number of projects a person is an active member in.
    periode of activity
    number of required skills in the personal profile
    number of project ToIs in the personal profile
    number of active members in the project, who cooperated in projects in the personal profile.
    number of previous members in the project, who cooperated in projects in the personal profile
    number of active members in the project, who the person follows
    number of previous members in the project, who the person follows
4. Projects are ranked by their score

Other parameters maybe found later.



