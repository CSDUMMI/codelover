
2024-01-06:
- Add register, login and logout functionality in auth.py
- Add database models in db.py
- Add routes for editing personal profiles
